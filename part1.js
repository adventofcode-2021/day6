const fs = require("fs");

class Fish {
  constructor(initTimer) {
    this.timer = initTimer;
  }
  nextDay() {
    if (this.timer == 0) {
      this.timer = 6;
      return new Fish(8);
    } else {
      this.timer--;
      return null;
    }
  }
}

const fishList = fs
  .readFileSync("input-part1.txt")
  .toString()
  .trim()
  .split(",")
  .map((x) => parseInt(x))
  .map((x) => new Fish(x));

for (let i = 0; i < 80; i++) {
  const newFish = fishList
    .map((x) => x.nextDay(fishList))
    .filter((x) => x != null);
  fishList.push(...newFish);
}
console.log(fishList.length);
