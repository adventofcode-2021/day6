const fs = require("fs");

let fishes = new Map();

fs.readFileSync("input-part2.txt")
  .toString()
  .trim()
  .split(",")
  .map((x) => parseInt(x))
  .forEach((x) => addFishToMap(x, fishes));

function addFishToMap(fish, map) {
  if (map.has(fish)) {
    map.set(fish, map.get(fish) + 1);
  } else {
    map.set(fish, 1);
  }
}

for (let i = 0; i < 256; i++) {
  const newFishes = new Map();
  [...fishes.keys()].forEach((x) => {
    newFishes.set(x - 1, fishes.get(x));
  });
  if (newFishes.has(-1)) {
    newFishes.set(8, newFishes.get(-1));
    newFishes.set(6, (newFishes.get(6) || 0) + newFishes.get(-1));
    newFishes.delete(-1);
  }
  fishes = newFishes;
}

const count = [...fishes.values()].reduce((a, b) => a + b);
console.log(count);
